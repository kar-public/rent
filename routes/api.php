<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', 'ApiController@test')->name('test');
Route::get('/properties', 'ApiController@properties')->name('properties');
Route::get('/properties/{id}', 'ApiController@property')->name('property');

Route::group(['middleware' => 'auth:api'], function () {
    // TODO
});
