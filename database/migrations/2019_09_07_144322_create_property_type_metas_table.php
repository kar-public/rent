<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTypeMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_type_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('property_type_id');
            $table->string('name', 128);

            $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete('cascade');
        });

        $metaData = [
            ['property_type_id' => 1, 'name' => 'студия'],
            ['property_type_id' => 1, 'name' => 'студию'],
            ['property_type_id' => 2, 'name' => 'комната'],
            ['property_type_id' => 2, 'name' => 'комнаты'],
            ['property_type_id' => 2, 'name' => 'комнату'],
            ['property_type_id' => 2, 'name' => 'комнат'],
            ['property_type_id' => 3, 'name' => 'квартира'],
            ['property_type_id' => 3, 'name' => 'квартиру'],
            ['property_type_id' => 3, 'name' => 'квартиры'],
            ['property_type_id' => 3, 'name' => 'квартир'],
            ['property_type_id' => 4, 'name' => 'частный дом'],
        ];

        foreach ($metaData as $item) {
            DB::table('property_type_metas')->insert($item);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_type_metas');
    }
}
