<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_vk')->nullable()->unique();
            $table->bigInteger('from_id')->nullable();
            $table->unsignedBigInteger('city_id')->default(0);
            $table->string('title', 256);
            $table->text('description');
            $table->integer('square')->nullable();
            $table->integer('price')->nullable();
            $table->string('address')->nullable();
            $table->string('image')->nullable();
            $table->string('phone')->nullable();
            $table->integer('room')->default(1);
            $table->unsignedBigInteger('type_id')->default(2);
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('property_types');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
