<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 128);
        });

        $types = [
            ['id' => 1, 'name' => 'студия'],
            ['id' => 2, 'name' => 'комната'],
            ['id' => 3, 'name' => 'квартира'],
            ['id' => 4, 'name' => 'частный дом'],
        ];

        foreach ($types as $type) {
            DB::table('property_types')->insert($type);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_types');
    }
}
