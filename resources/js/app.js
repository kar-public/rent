import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import Router from './routes.js'

import VueResource from 'vue-resource'

import Auth from './packages/auth/Auth.js'

Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(Auth);

if (Vue.auth.getToken()) {
    Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken();
}

const store = new Vuex.Store({
    state: {

    },
    getters: {
    },
    mutations: {
    },
    actions: {}
});

Router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.isAuthenticated()) {
                next({
                    path: '/'
                })
            } else next()
        } else if (to.matched.some(record => record.meta.forAuth)) {
            if (!Vue.auth.isAuthenticated()) {
                next({
                    path: '/home'
                })
            } else next()
        } else next()
    }
)

new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router: Router
})
