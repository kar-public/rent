<!DOCTYPE html>
<html lang="en">
<head>
    <title>Royal Estate - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="themes/royalestate-master/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="themes/royalestate-master/css/animate.css">

    <link rel="stylesheet" href="themes/royalestate-master/css/owl.carousel.min.css">
    <link rel="stylesheet" href="themes/royalestate-master/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="themes/royalestate-master/css/magnific-popup.css">

    <link rel="stylesheet" href="themes/royalestate-master/css/aos.css">

    <link rel="stylesheet" href="themes/royalestate-master/css/ionicons.min.css">

    <link rel="stylesheet" href="themes/royalestate-master/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="themes/royalestate-master/css/jquery.timepicker.css">


    <link rel="stylesheet" href="themes/royalestate-master/css/flaticon.css">
    <link rel="stylesheet" href="themes/royalestate-master/css/icomoon.css">
    <link rel="stylesheet" href="themes/royalestate-master/css/style.css">
    <!-- Place your kit's code here -->
    <script src="https://kit.fontawesome.com/89c4dd1cbc.js"></script>
</head>
<body>
<!-- END nav -->

<div id="app">
</div>

<script src="{{ mix('js/app.js') }}"></script>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>

<script src="themes/royalestate-master/js/jquery.min.js"></script>
<script src="themes/royalestate-master/js/jquery-migrate-3.0.1.min.js"></script>
{{--<script src="themes/royalestate-master/js/popper.min.js"></script>--}}
<script src="themes/royalestate-master/js/bootstrap.min.js"></script>
{{--<script src="themes/royalestate-master/js/jquery.easing.1.3.js"></script>--}}
<script src="themes/royalestate-master/js/jquery.waypoints.min.js"></script>
<script src="themes/royalestate-master/js/jquery.stellar.min.js"></script>
<script src="themes/royalestate-master/js/owl.carousel.min.js"></script>
<script src="themes/royalestate-master/js/jquery.magnific-popup.min.js"></script>
<script src="themes/royalestate-master/js/aos.js"></script>
{{--<script src="themes/royalestate-master/js/jquery.animateNumber.min.js"></script>--}}
<script src="themes/royalestate-master/js/bootstrap-datepicker.js"></script>
{{--<script src="js/jquery.timepicker.min.js"></script>--}}
<script src="themes/royalestate-master/js/scrollax.min.js"></script>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>--}}
{{--<script src="themes/royalestate-master/js/google-map.js"></script>--}}
<script src="themes/royalestate-master/js/main.js"></script>

</body>
</html>
