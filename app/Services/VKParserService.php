<?php


namespace App\Services;


use App\Models\Metro;
use App\Models\Property;
use App\Models\PropertyTypeMeta;
use App\Models\PropertyTypes;

class VKParserService
{
    /**
     * @var \stdClass
     */
    protected $data;

    /**
     * VKParserService constructor.
     * @param \stdClass $data
     */
    public function __construct(\stdClass $data)
    {
        $this->data = $data;
    }

    public function parseMetro(int $cityId): ?Metro
    {
        $metros = Metro::all()->where('city_id', $cityId)->keyBy('name')->keys();
        $regex = '/(метро|м|м\.|метро\s+пр\.|проспект)\s?(' . implode('|', $metros->toArray()) . ')/imu';
        preg_match_all($regex, $this->data->text, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            list(, , $m) = current($matches);
            $metro = Metro::whereName($m)->first();
            if (!empty($metro)) {
                return $metro;
            }
        }

        $regex = '/(' . implode('|', $metros->toArray()) . ')/imu';
        preg_match_all($regex, $this->data->text, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            list(, $m) = current($matches);
            $metro = Metro::whereName($m)->first();
            if (!empty($metro)) {
                return $metro;
            }
        }
        return null;
    }

    public function parsePhone(): ?String
    {
        $regex = '/(\+7|8)\d{10}/m';
        preg_match_all($regex, $this->data->text, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            list($phone) = current($matches);
            return preg_replace('/^8/', '+7', $phone);
        }
        return '';
    }

    public function parseSquare(): ?int
    {
        $regex = '/((\d+\s*))(метров|м2)/imu';
        preg_match_all($regex, $this->data->text, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            list(, $square) = current($matches);
            return intval($square);
        }
        return null;
    }

    public function parsePrice()
    {
        $regexList = [
            '/(стоимость|арендная\s*плата|цена|оплата|аренда|(цена)?\s*(за|в)\s*месяц)\:*\s*(((\d+\s*)+(тис|т\.*\s*руб|т\.*\s*р\.*|т))|(\d+\s*)+)/imu',
            '/((\d+\s(тис|т\.*\s*руб|т\.*\s*р\.*|т))|(\d+\s*)+|(\d+\.\s*\d*\s*))(руб|rub|₽|в\s*месяц|\+ку|\+к\/у)/imu',
        ];
        foreach ($regexList as $regex) {
            preg_match_all($regex, $this->data->text, $matches, PREG_SET_ORDER, 0);
            if (count($matches) > 0) {
                list($price) = current($matches);
                $price = preg_replace('/(тис|т\.*\s*руб|т\.*\s*р\.*|т)/imu', '000', $price);
                preg_match_all('!\d+!', $price, $matches);
                $price = number_format(implode("", current($matches)));
                return $price;
            }
        }
        return '';
    }

    public function parsePropertyType(): ?PropertyTypes
    {
        $types = PropertyTypeMeta::all()->keyBy('name')->keys();
        $regex = '/(сдам|сдается|сдаётся)\s*\t*(' . implode('|', $types->toArray()) . ')/imu';
        preg_match_all($regex, $this->data->text, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            list(, , $m) = current($matches);
            $typeMeta = PropertyTypeMeta::whereName($m)->first();
            $type = PropertyTypes::whereId($typeMeta->property_type_id)->first();
            if (!empty($type)) {
                return $type;
            }
        }
        return null;
    }

    public function getImage(array $attachments, $type = 'q')
    {
        $types = ['s', 'm', 'x', 'y', 'z'];
        if (isset($attachments['photo']['sizes'])) {
            foreach ($attachments['photo']['sizes'] as $item) {
                if ($type == $item['type']) {
                    return $item;
                }
            }
        }
        foreach ($attachments['photo']['sizes'] as $item) {
            foreach (array_reverse($types) as $val) {
                if ($val == $item['type']) {
                    return $item;
                }
            }
        }
        return '';
    }

    public function parse() : Property
    {
        $property = new Property();
        $property->metro = $this->parseMetro(1)->name ?? '';
        $property->phone = $this->parsePhone();
        $property->price = $this->parsePrice();
        $property->square = $this->parseSquare() ?? '';
        $property->type = $this->parsePropertyType()->name ?? '';
        $property->text = $this->data->text;
        $property->id_vk = $this->data->id;
        // TODO increment
        $property->id = $this->data->id;
        $property->from_id = $this->data->from_id;
        $property->date = date('Y-m-d H:i:s', $this->data->date);
        if (isset($this->data->attachments)) {
            $property->image = $this->getImage(current($this->data->attachments)) ?? '';
        }
        return $property;
    }

    public function parseAttachments(String $type = 'q') : array
    {
        $attachments = [];
        if (isset($this->data->attachments)) {
            foreach ($this->data->attachments as $attachment) {
                $attachments[] = $this->getImage($attachment, $type) ?? '';
            }
        }
        return $attachments;
    }

}
