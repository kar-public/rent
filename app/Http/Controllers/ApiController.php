<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Services\VKParserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use VK\Client\VKApiClient;

class ApiController extends Controller
{
    protected $translateService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }


    public function test(Request $request)
    {
        return response()->json(
            DB::table('users')
                ->select('name', 'id')
                ->get()->toArray()

        );
    }

    public function properties(Request $request)
    {
        $group_id = 28618880;
        $topic_id = 40497755;
        $perPage = $request->get('perPage', 24);
        $page = $request->get('page', 1);
        // TODO
        $access_token = env('VK_ACCESS_TOKEN');
        $vk = new VKApiClient();
        $data = $vk->board()->getComments($access_token, array(
            'v' => '5.95',
            'group_id' => $group_id,
            'topic_id' => $topic_id,
            'fields' => 'bdate',
            'sort' => 'desc',
            'count' => $perPage,
            'offset' => ($page - 1) * $perPage,
            'start_comment_id' => 90781,
        ));
        $response = ['items' => [], 'count' => $data['count']];
        foreach ($data['items'] as $item) {
            $vkParser = new VKParserService((object)$item);
            $property = $vkParser->parse();
//            $property->save();
            $response['items'][] = $property->toArray();
        }
        return response()->json($response);
    }

    public function property(Request $request, $id)
    {
        $group_id = 28618880;
        $topic_id = 40497755;
        // TODO
        $access_token = env('VK_ACCESS_TOKEN');
        $vk = new VKApiClient();
        $data = $vk->board()->getComments($access_token, array(
            'v' => '5.95',
            'group_id' => $group_id,
            'topic_id' => $topic_id,
            'fields' => 'bdate',
            'sort' => 'desc',
            'count' => 1,
            'start_comment_id' => $id,
        ));
        $item = current($data['items']);
        $vkParser = new VKParserService((object)$item);
        $property = $vkParser->parse();
        $attachments = $vkParser->parseAttachments('z');
        return response()->json(array_merge(['attachments' => $attachments], $property->toArray()));
    }

}
