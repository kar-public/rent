<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'id_vk', 'from_id', 'city_id', 'title', 'description', 'square', 'price', 'address', 'image', 'phone', 'room', 'type_id',
    ];

}
